# Tenant Manager

## General
Tenant Manger is a component developed by Opplafy with APInf in Tampere smart city project. It is not a FIWARE component.

Tenant manager is a middleware orchestration component. It enables OAuth2 token usage by taking requests from API Management for Tenants and configures Keyrock IDM and Umbrella Proxy accordingly.

Alternatively, if tenant manager is not used, the Umbrella Proxy sub URL configurations need to be done manually. Tenant manager integration removes this problem.

Original documentation can be found here: https://github.com/opplafy/tenant-manager

## Usage of Tenant manager
Tenant manager is a used to update Keyrock and Umbrella configuration automatically when a Tenant is created in API Management. When Tenant manager recieves a request to the API endpoints, it will POST a JSON payload to Keyrock to request an Organisation creation. After this, it will post a JSON to Umbrella to request a Sub-URL rule creation to Context broker API. Delete/modify operations will work in a similar fashion.

Use of Tenant manager is instructed in the [platform-testing guide](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/tree/master/platform-testing#21-tenant-user-creation). 

Or you can watch this Youtube-Video: https://www.youtube.com/watch?v=2s_WkM8gIug

## Versioning  
Tested on:  
Version profirator/tenant-manager (DockerHub digest: sha256:a0b416e2596405de6cc5e9ebbd3db519042fc4cc967f4781f7000a922ad505b1)

## Volume Usage
This component does not use persistent volumes

## Load-Balancing
Currently only one Replica is deployed.

## Route (DNS)
There is no outside route to this component. It is part of the Apinf Platform.

## Further Testing
The image is taken as is. No further component testing is done.

## Deployment
In order to deploy, following Gitlab-Variables and K8s-secrets are needed:
### Gitlab-Variables
GITLAB_ACCESS_TOKEN - This variable contains the token used to access Gitlab Docker image registry  
KUBECONFIG - This variable contains the content of the Kube.cfg used to access the cluster for dev- and staging-stage.  
KUBECONFIG_PROD - This variable contains the content of the Kube.cfg used to access the cluster for prod-stage.  
NAMESPACE_DEV - This variable contains the namespace for dev-stage used by k8s  
NAMESPACE_STAGING - This variable contains the namespace for staging-stage used by k8s  
NAMESPACE_PROD - This variable contains the namespace for production-stage used by k8s  
TENANT_DEV_DOMAIN - This variable contains the domain name used for the component in dev-stage. Ex. example.com  
TENANT_PROD_DOMAIN - This variable contains the domain name used for the component in production-stage. Ex. example.com   
TENANT_STAGING_DOMAIN - This variable contains the domain name used for the component in staging-stage. Ex. example.com   
  
### Kubernetes Secrets
Create kubernetes-secret for gitlab registry credentials  
`kubectl create secret docker-registry gitlab-registry --docker-server=GITLABADDRESS --docker-username=GITLABUSER --docker-password=GITLABTOKEN --namespace=fiware-dev|fiware-staging|fiware-prod`  

Create kubernetes-secret for tenantmanager credentials  
```cat <<EOF >credentials.json
{
    "bae": {
        "client_id": "Market-ClientID"
    },
    "broker": {
        "client_id": "APICatalogue-ClientID"
    },
    "idm": {
        "user_id": "admin",
        "user": "KEYROCK-ADMIN-EMAIL",
        "password": "KEYROCK-ADMIN-PASSWORD"
    },
    "umbrella": {
        "token": "UMBRELLA-ADMIN-APITOKEN",
        "key": "UMBRELLA-API_USER-APIKEY"
    }
}
EOF
```

`kubectl create secret generic tenantmanager-credentials --from-file=credentials.json --namespace=fiware-dev|fiware-staging|fiware-prod`

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE](LICENSE) for additional information.
